(*
	So in this graphics test we're not using 'open' and we're also using the 'Random' module. This means
	every time we wish to use any functions of either of these modules we must prefix the function calls
	with 'Graphics' or 'Random'. 
	These first two lines initialise the random number generator and open a graphics window.
*)
Random.init 5;;
Graphics.open_graph "640x480";;

(* Iterative function.*)
let rec iterate r x_init i =
	(* If 'i' has reached 1 then return init_x and break out of recursion.*)
	if i = 1 then 
		x_init
	else
		let x = iterate r x_init (i-1) in
		r *. x *. (1.0 -. x);;

(*
	The purpose of this loop is to plot 40 points for each pixel along the x axis; the code within the
	loop is used to determine where along the y axis each point is plotted.
*)		
for x = 0 to 639 do
	(* Retrieve a number 'r' ranging from 0.0 to 4.0 as the loop continues.*)
	let r = 4.0 *. (float_of_int x) /. 640.0 in
	(* Loop 40 times to plot 40 different points.*)
	for i = 0 to 39 do
		(* Get a random float from between 0.0 and 1.0*)
		let x_init = Random.float 1.0 in
		(* Call the recursive iterate function passing in our 'r', then our random number, and finally 500.*)
		let x_final = iterate r x_init 500 in
		let y = int_of_float (x_final *. 480.) in
		Graphics.plot x y
	done
done;;

read_line();;

(*
	I gace in trying to understand this.
*)