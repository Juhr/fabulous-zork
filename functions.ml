(*
  Comments are a little weird, and the parser has very good type inferring.
  Things to remember:
  Types are inferred.
  No conversion between ints and floats, must use +,-,*,/ for ints and +.,-.,*.,/. for floats.
  No return keyword, last expression is the return value.
*)
let average a b = 
	(a +. b) /. 2.0;;
	
(*
  Also, functon arguments are given without parentheses or commas, but
  when a function call is an argument the whole thing is given in parens.
*)
print_float (average 10. 5.);;
print_endline "";;

(*
  Explicit casts must be made for ints and floats.
*)
let int_plus_float i f =
	(float_of_int i) +. f;;
let float_plus_int f i =
	(int_of_float f) + i;;
	
(*
  Same must be done with strings.
*)
let s = String.concat "" ["int to float: "; string_of_float(int_plus_float 5 5.0)];;
(*Or we could concat the simple way...*)
let s = "int to float: " ^ string_of_float(int_plus_float 5 5.0);;
print_endline s

(*Looks like it rounds down.*)
let s = "float to int: " ^ string_of_int(float_plus_int 7.99999 2);;
print_endline s;;

(*'float_of_int' also has a nice short hand*)
print_float (float 5);;
print_endline "";;

(*
	A recursive function call requires you to explicitly add 'rec'.
	This function returns a list of ints ranging from a to b.
	'then' returns an empty list, and 'else' returns a list with a
	as its head and the result of a recursive call of range as its
	tail.
*)
let rec range a b =
	if a > b then
		[]
	else
		a :: range(a + 1) b;;

(*
	This recursive function also does argument matching.
	Remember: Always end pattern matches with ;; or you'll very silly bugs.
*)
let rec print_list_int = function
	| [] -> print_string "End"	(*Matched empty list, return nothing.*)
	| e::l -> 	print_int e;		(*Matched a list (at least a head), print the head int.*)
				print_string " ";	(*Print a space to separate this head and the next.*)
				print_list_int l;;	(*Recursively call print_list_int to print the rest of the list.*)
print_list_int (range 0 10);;
print_endline "";;

(*
	The reason for the 'rec' keyword is because without it the function wouldn't be defined
	by the time its recursive call to itself was being parsed. So, as below, if there is already
	a definition of the function, the first definition will beused when it is called in the second
	definition if the 'rec' keyword isn't used.
*)
let time_travel a =
	print_string ("The first function doubles the number: " ^ string_of_int (a * 2) ^ "\n");;
let time_travel a =
	time_travel a;
	print_string("and the second function triples the number: " ^ string_of_int(a * 3) ^ "\n");;
time_travel 2;;
time_travel 3;;