(*
	This is how you make a local variable. Think of the "let ... in" as a statement in itself
	but only for this expression, that's why we don't have a semicolon on the end of it.
	Here 'sum' is not actually an assigned variable, like you might do in Java, instead it
	is just shorthand for the expression it's given and is never placed on the stack.
*)
let average a b =
	let sum = a +. b in
	sum /. 2.0;;
	
(*
	This is an example of the use of "let ... in". Apparently the second one which actually
	uses is is faster.
*)
let f a b =
	(a +. b) +. (a +. b) ** 2.;;
let f a b =
	let x = a +. b in
	x +. x ** 2.0;;
	
(*
	Any use of 'let', whether it's in on the top-level of within a function is called a 'let-binding'.
*)

(*
	This is how you create a reference to an integer in OCaml (a real variable). The first line will
	simply create an integer referene and then throw it away, since we didn't atually do anything with
	it. The second line will create the reference and then store it in 'my_ref', and then the third
	line puts the value 100 into the reference (Notice the := used to assign the value). And finally we
	print the reference's contents to see that it's all worked, notice the ! used to dereference.
*)
ref 0;;
let my_ref = ref 0;;
my_ref := 100;;
print_int !my_ref;;
print_newline();;

(*
	This is how one would go about doing a nested function. Here a function is defined between a
	"let ... in", and then it is used twice to produce the output.
*)
let nested a = 
	let double =
		a * 2
	in
	double + double;;
print_int (nested 1);;
print_newline();;

