(*
	This program is about when to and when not to use single and double semicolons.
	
	NOTE: a single semicolon is known as a "sequence point". Its use is the same as in
	Java and C-like languages and means "Do the stuff before this point before you do
	anything after this point".
	
	1). Doubles are used to separate statements at the top level, but never within function
	definitions or any other kind of statement.
	
	2). Sometimes you can omit doubles, but I shouldn't do this myself while I'm beginning. The
	places you can do it is with 'let', 'open', 'type', and at the end of a file (among some other
	places).
	
	3). "let ... in" should be considered a statement itself and should never have a semicolon after it.
	
	4). All other statements within a block of code should end with a single semicolon, except for the 
	very last one. In some code blocks (like "for, do, done") we don't need anything, but for others 
	(like function definitions), we need double semicolons to denote the end.
*)

(* These two statements are on the top level, so they're separated by doubles.*)
Random.self_init();;
open Graphics;;

(* Here a double has been omitted. I assume this is because 'let' can only every come after the end
of a statement and so the compiler assumes the previous statement is finished.*)
open_graph "640*480" (* Omitted ;;*)
let a_number = 500;;

(* This gives an example of "let ... in" being treated as a statement itself, and ending a function
with double semicolons.*)
let awkward_triple a =
	let double = a * 2 in
	double + a;;
	
(*
	There's also some very confusing stuff about a single semicolon being an expression in Ocaml with
	the type unit -> 'b -> 'b which takes two values and simply returns the second one. I'm not sure
	I can can get my head around this right now so I won't worry too much about it.
*)