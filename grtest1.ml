(*
	Loads the graphics module. Without 'open' all calls to graphics functions would require a
	'Graphics.' prefix. I find this comparable with C#'s 'using...'.
*)
open Graphics;;

(*
	Opens a window with the given size.
*)
open_graph "640x480";;

(*
	Draws circles which decrease in size and alternate between red and yellow.
*)
for i = 12 downto 1 do
	let radius = i * 20 in
	set_color (if i mod 2 = 0 then red else yellow);
	fill_circle 320 240 radius
done;;

(*
	Obvious read line to stop the program from closing. Though when I do close the program via
	the window's 'X' the program crashes.
*)
read_line();;